# OtoScrapper

OtoScrapper jest automatycznym narzędziem do zapisywania skategoryzowanych już ogłoszeń mieszkań i domów
dostępnych na portalu otoDom. 

Skrypt wyszukuje ogłoszenia według filtrów użytkownika, z których następnie uzyskuje informację w takich kategoriach jak:
* Miasto
* Dzielnica
* Powierzchnia
* Cena
* Cena za metr
* Mieszkanie / Dom
* Rodzaj zabudowy
* Rynek (wtórny/pierwotny)
* Forma własności
* Liczba pokoi
* Piętro
* Czynsz
* Ogrzewanie
* Rok budowy
* Okna (plastikowe/drewniane)
* Link do ogłoszenia
#
Wszystkie te informacje zapisuje do pliku .xlsx w którym wówczas można sortować ogłoszenia wedle uznania lub przeglądać je wszystkie w wygodnej strukturze danych. 

## Instalacja

Aby uruchomić skrypt potrzebny będzie Python w ver 3.8.6.
Pobierz repozytorum za pomocą gita
```bash
git clone https://gitlab.com/oskar_rokita/otodom-scrapper.git
```
Lub pobierz w spakowane w ZIP na stronie repozytorium. Rozpakuj pliki.

Następnie w folderze projektu użyj :

```bash
python -m pip install -r requirements.txt
```
Możliwe jest też używanie bez instalowania Pythona. Dostepny jest plik .exe który wystarczy uruchomić.

## Używanie
Skrypt wyposażony jest w GUI , za pomocą którego wybiera się filtry wyszukiwania ogłoszeń. 
GUI wyświetla również komunikaty odnoście stanu programu oraz paski postępu.
#
Uruchom skrypt komendą :
```python
python OTOscrapper.py
```
lub dwukrotnym kliknięciem w plik.

Po uruchomieniu się okna graficznego wybierz filtry za pomocą których chcesz szukać ogłoszeń i kliknij "Wyszukaj"

Skrypt wyświetli ile ogłoszeń spełniających wymagania udało mu się znaleźć. Następnie wybierz ile z nich chcesz pobrać i kliknij "Pobierz". Gdy pasek progresu dojdzie do 100% można zamknąć okno Scrappera. Plik xlsx z pobranymi ogłoszeniami będzie w tym samym folderze w którym uruchomiony został skrypt. W nazwie doklejana jest data powstania pliku.

## Rozwój
Planowane jest rozwinięcie projektu o nowe funkcję oraz udoskonalenie obecnych.

## License
[MIT](https://choosealicense.com/licenses/mit/)
