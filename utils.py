def only_digits(text):
    number = ''
    text = text.replace(',','')
    for l in text.replace(' ',''):
        if l.isdigit():
            number += l
    try:
        if int(number)>10000000:
            return int(number/100)
        return int(number)
    except:
        return None

wanted_divs = {'Powierzchnia':'',
                'Cena':'',
                'Cena za metr':'',
                'Miasto':'',
                'Dzielnica':'',
                'Rynek':'',
                'Rodzaj zabudowy':'',
                'Forma własności':'',
                'Liczba pokoi':'',
                'Stan wykończenia':'',
                'Piętro':'',
                'Czynsz':'',
                'Ogrzewanie':'',
                'Rok budowy':'',
                'Okna':'',
                'Link':''
                    }

rooms_dict = {
        '1':'ONE',
        '2':'TWO',
        '3':'THREE',
        '4':'FOUR',
        '5':'FIVE',
        '6':'SIX',
        '7':'SEVEN',
        '8':'EIGHT',
        '9':'NINE'
    }

city_select_list = ['','Gdańsk', 'Warszawa', 'Poznań','Gdynia', 'Olsztyn','Biskupiec','Bartoszyce']
flat_select_list = ['','Mieszkanie','Dom']
rooms_select_list = [_ for _ in range(1,9)]
prize_select_list = [str(x)+' 000' for x in range(100, 2000, 100)]
area_select_list = [str(x)+'m²' for x in range(20,150,10)]