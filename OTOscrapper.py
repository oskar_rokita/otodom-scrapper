from requests import get as RequestGet
from xlsxwriter import Workbook
from xlrd import open_workbook
from bs4 import BeautifulSoup
from datetime import datetime
from threading import Thread
from tkinter.ttk import *
from tkinter import *
import unidecode
from utils import (
    wanted_divs,
    only_digits,
    rooms_dict,
    city_select_list,
    flat_select_list,
    rooms_select_list,
    prize_select_list,
    area_select_list
    )

class OTOSCRAPPER:
    def __init__(self) -> None:
        self.file_name = 'OTO-Scrapper-'+datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
        self.base_html = 'https://www.otodom.pl/pl/oferty/sprzedaz/'
        self.data = wanted_divs
        self.links_number = 0
        self.links_list = []
        self.links_to_download = 1
        self.user_filters= ''
        self.counter = 0
        self.debug = 0

        self.root = Tk()
        self.root.title('OTOScrapper')


    def create_file(self):
        self.workbook = Workbook(f'{self.file_name}.xlsx')
        worksheet = self.workbook.add_worksheet()
        col = 0
        for _ in self.data.keys():
            worksheet.write(0, col, _)
            col += 1
    
        self.workbook.close()


    def links_count(self, url):
        r = RequestGet(url)
        soup = BeautifulSoup(r.content, 'html.parser')
        div = "search.listing-panel.label.ads-number"
        content = soup.select(f'strong[data-cy="{div}"]')

        self.links_number = int(content[-1].text.split('}')[-1])
        
        return self.links_number


    def get_links_list(self, url):
        import re

        r = RequestGet(url)
        soup = BeautifulSoup(r.content, 'html.parser')
        self.links_list = []

        data = str(soup)
        x = re.findall(r'href="(.*?)"><aside',data)
        for link in x:
            if 'oferta' in link and len(link)<400:
                self.links_list.append(link)

            

    def save_data(self):
        wb = Workbook(f'{self.file_name}.xlsx')
        wbRD = open_workbook(f'{self.file_name}.xlsx')
        sheets = wbRD.sheets()
        for sheet in sheets: # write data from old file
            newSheet = wb.add_worksheet(sheet.name)
            for row in range(sheet.nrows):
                for col in range(sheet.ncols):
                    newSheet.write(row, col, sheet.cell(row, col).value)
                    
        for i, (k,v) in enumerate (self.data.items()):
            newSheet.write(sheet.nrows,i,v)
        newSheet.set_column('A:N', 16)
        newSheet.set_column('P:R', 100)
        wb.close()


    def get_single_data(self, url):
        r = RequestGet(url)
        soup = BeautifulSoup(r.content, 'html.parser')

        for div in self.data.keys():
            content = soup.select(f'div[aria-label="{div}"]')
            if content:
                try:
                    self.data[div] = content[0].find_all('div', class_='css-1wi2w6s estckra5')[0].text
                except:
                    continue
        try:
            cena = str(soup.select('strong[data-cy="adPageHeaderPrice"]')[0].text)
        except:
            return False


        self.data['Cena'] = only_digits(cena)
        
        if self.data['Cena'] == None:
            return False
        
        self.data['Cena za metr'] = round(self.data['Cena']/only_digits(self.data['Powierzchnia'][0:2]),2)
        adres = soup.select('a[aria-label="Adres"]')[0].text
        self.data['Dzielnica'] = adres.split()[1].replace(',','')
        self.data['Miasto'] = adres.split()[0].replace(',','')
        self.data['Link'] = url

        return True


    def data_scrapping_loop(self, links_to_download):

        self.info_var.set("Trwa pobieranie, nie zamykaj okna")
        page = 1
        while self.counter <= links_to_download:
            self.get_links_list(self.base_html + self.user_filters + f'&page={page}')

            for link in self.links_list:
                if self.get_single_data(f'https://www.otodom.pl{link}'):
                    self.save_data()
                    self.counter += 1
            
                self.progress_label['text'] = self.update_progess_label()

                if self.counter == links_to_download:
                    break
            
            page += 1
            

    def update_progess_label(self):
        self.pb['value'] =  round(100/self.links_to_download*(self.counter),2)
        if self.pb['value'] == 100:
            self.info_var.set("Gotowe!")
            self.root.update_idletasks()
        return f"Obecny postęp: {self.pb['value']}%"


    def start_grind(self, links_num):
        self.links_to_download = links_num - 1
        Thread(target=self.data_scrapping_loop, args=[links_num],).start()


    def create_link(self):
        miasto = self.city.get()
                
        miasto = unidecode.unidecode(miasto)
        prizeMax = self.prize_max.get().replace(' ','')
        prizeMin = self.prize_min.get().replace(' ','')
                
        areaMin = self.area_min.get()[:-2]
        areaMax = self.area_max.get()[:-2]
        roomsVal = rooms_dict[self.rooms.get()]
        self.user_filters= f'{self.flat.get().lower()}/{miasto}?priceMin={prizeMin}&priceMax={prizeMax}&areaMin={areaMin}&areaMax={areaMax}&roomsNumber=%5B{roomsVal}%5D'
        links_number = self.links_count(self.base_html+self.user_filters)
        self.info_var.set(f'Wykryto {links_number} ogłoszeń, ile chcesz pobrać?')
        self.root.update_idletasks()

        links_num = IntVar()
        links_slider = Scale(self.root, from_=1, to=links_number,label='Ilość ogłoszeń', orient='horizontal',variable=links_num)
        links_slider.grid(row=3,column=1)

        Button(self.root,text='Pobierz',
               font=('arial', 12, "bold"),
               command = lambda: self.start_grind(links_num.get())).grid(row=3,column=2,padx=5,pady=10)

        self.pb = Progressbar(self.root, orient='horizontal',mode='determinate',length=400)
        self.pb.grid(row=5,column=0,columnspan=4, padx=10, pady=20)

        self.progress_label = Label(self.root, text = self.update_progess_label())
        self.progress_label.grid(row=4, column=1, columnspan=2)


    def GUI(self):
        self.info_var = StringVar()
        self.city = StringVar(self.root)
        self.flat = StringVar(self.root)
        self.prize_min = StringVar(self.root)
        self.prize_max = StringVar(self.root)
        self.area_min = StringVar(self.root)
        self.area_max = StringVar(self.root)
        self.rooms = StringVar(self.root)
        self.buttonText = StringVar(self.root)

        self.info_var.set('Wybierz filtry wyszukiwania')
        if self.debug:
            self.city.set('Gdańsk')
            self.flat.set('Mieszkanie')
            self.prize_min.set('100000')
            self.prize_max.set('400000')
            self.area_min.set('20')
            self.area_max.set('40')
            self.rooms.set('3')
        else:
            self.city.set('Miasto')
            self.flat.set('Mieszkanie')
            self.prize_min.set('Cena od')
            self.prize_max.set('do')
            self.area_min.set('Powierzchnia od')
            self.area_max.set('do')
            self.rooms.set('Pokoje')
        self.buttonText.set('Wyszukaj')

        Label(self.root, textvariable=self.info_var,font=('arial', 15)).grid(row=0,column=0,columnspan=4)
        city_menu = OptionMenu(self.root, self.city, *city_select_list)
        city_menu.config(width=15)
        city_menu.grid(row=1,column=0,padx=5,pady=10)
        OptionMenu(self.root, self.flat, *flat_select_list).grid(row=1,column=1,padx=5,pady=10)
        OptionMenu(self.root, self.rooms, *rooms_select_list).grid(row=1,column=2,padx=5,pady=10)
        Button(self.root,textvariable=self.buttonText,font=('arial', 12, "bold"), command = self.create_link).grid(row=1,column=3,padx=5,pady=10)
        OptionMenu(self.root, self.prize_min, *prize_select_list).grid(row=2,column=0,)
        OptionMenu(self.root, self.prize_max, *prize_select_list).grid(row=2,column=1)
        OptionMenu(self.root, self.area_min, *area_select_list).grid(row=2,column=2)
        OptionMenu(self.root, self.area_max, *area_select_list).grid(row=2,column=3)

        self.root.mainloop()


    def main(self):
        self.create_file()
        self.GUI()


if __name__ == '__main__':
    scrapper = OTOSCRAPPER()
    scrapper.main()
